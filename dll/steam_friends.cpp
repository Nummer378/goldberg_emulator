#include "base.h"
#include "steam_friends.h"
#include <string>

bool loadOwnAvatar(unsigned char* smallBuf, unsigned char* mediumBuf, unsigned char* largeBuf, Local_Storage* local_storage) {
	// Load own avatar from file (avatar.* in %AppData%\Goldberg SteamEmu Saves\settings on Windows, similar path's on other OS'es)
	PRINT_DEBUG("loadOwnAvatar() called!\n");
	std::string supported_extensions[5] = {".png", ".jpg", ".jpeg", ".bmp", ".gif"};
	int array_length = 5;
	int image_width = 0;
	int image_height = 0;
	std::string filename;
	unsigned char* image_data;
	for (int i = 0; i < array_length; i++) {
		// Construct filename we try to load
		filename = local_storage->get_global_settings_path() + "avatar" + supported_extensions[i];
		// Load file into memory (RGBA/4-channel format), assuming it exists (will fail if not)
		image_data = loadImageFromFile(filename.c_str(), &image_width, &image_height);
		if (image_data == NULL) {
			PRINT_DEBUG("Steam_Friends::loadOwnAvatar(): Failed to load avatar %s\n", filename.c_str());
			continue; // Try another extension
		}

		// Image was successfully loaded, now we need to resize it to the correct values required by the Steam API
		int returncodes = 0;
		returncodes += resizeImage(image_data, image_width, image_height, smallBuf, 32, 32);
		returncodes += resizeImage(image_data, image_width, image_height, mediumBuf, 64, 64);
		returncodes += resizeImage(image_data, image_width, image_height, largeBuf, 184, 184);
		if (returncodes != 3) {
			PRINT_DEBUG("Steam_Friends::loadOwnAvatar(): Failed to resize avatar!\n");
			continue; // Let's see if we got any better avatars in the folder
		}
		return true;
	}
	return false;
}