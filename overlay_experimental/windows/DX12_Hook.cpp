#include "DX12_Hook.h"
#include "Windows_Hook.h"
#include "../Renderer_Detector.h"
#include "../../dll/dll.h"

#ifdef EMU_OVERLAY

#include <imgui.h>
#include <impls/windows/imgui_impl_dx12.h>

#include <dxgi1_4.h>

DX12_Hook* DX12_Hook::_inst = nullptr;

bool DX12_Hook::start_hook()
{
	bool res = true;
	if (!hooked)
	{
		if (!Windows_Hook::Inst()->start_hook())
			return false;

		PRINT_DEBUG("Hooked DirectX 12\n");
		hooked = true;

		Renderer_Detector::Inst().renderer_found(this);

		BeginHook();
		HookFuncs(
			std::make_pair<void**, void*>(&(PVOID&)DX12_Hook::Present, &DX12_Hook::MyPresent),
			std::make_pair<void**, void*>(&(PVOID&)DX12_Hook::ResizeTarget, &DX12_Hook::MyResizeTarget),
			std::make_pair<void**, void*>(&(PVOID&)DX12_Hook::ResizeBuffers, &DX12_Hook::MyResizeBuffers),
			std::make_pair<void**, void*>(&(PVOID&)DX12_Hook::ExecuteCommandLists, &DX12_Hook::MyExecuteCommandLists)
		);
		EndHook();

		get_steam_client()->steam_overlay->HookReady();
	}
	return res;
}

void DX12_Hook::resetRenderState()
{
	if (initialized)
	{
		pSrvDescHeap->Release();
		for (UINT i = 0; i < bufferCount; ++i)
		{
			pCmdAlloc[i]->Release();
			pBackBuffer[i]->Release();
		}
		pRtvDescHeap->Release();
		delete[]pCmdAlloc;
		delete[]pBackBuffer;

		ImGui_ImplDX12_Shutdown();
		Windows_Hook::Inst()->resetRenderState();
		ImGui::DestroyContext();

		initialized = false;
	}
}

// Try to make this function and overlay's proc as short as possible or it might affect game's fps.
void DX12_Hook::prepareForOverlay(IDXGISwapChain* pSwapChain)
{
	if (pCmdQueue == nullptr)
		return;

	ID3D12CommandQueue* pCmdQueue = this->pCmdQueue;

	IDXGISwapChain3* pSwapChain3 = nullptr;
	DXGI_SWAP_CHAIN_DESC sc_desc;
	pSwapChain->QueryInterface(IID_PPV_ARGS(&pSwapChain3));
	if (pSwapChain3 == nullptr)
		return;

	pSwapChain3->GetDesc(&sc_desc);

	if (!initialized)
	{
		UINT bufferIndex = pSwapChain3->GetCurrentBackBufferIndex();
		if (pSwapChain3->GetDevice(IID_PPV_ARGS(&pDevice)) != S_OK)
			return;

		bufferCount = sc_desc.BufferCount;

		mainRenderTargets.clear();

		{
			D3D12_DESCRIPTOR_HEAP_DESC desc = {};
			desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
			desc.NumDescriptors = 2;
			desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
			if (pDevice->CreateDescriptorHeap(&desc, IID_PPV_ARGS(&pSrvDescHeap)) != S_OK)
			{
				pDevice->Release();
				pSwapChain3->Release();
				return;
			}
		}
		{
			D3D12_DESCRIPTOR_HEAP_DESC desc = {};
			desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
			desc.NumDescriptors = bufferCount;
			desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
			desc.NodeMask = 1;
			if (pDevice->CreateDescriptorHeap(&desc, IID_PPV_ARGS(&pRtvDescHeap)) != S_OK)
			{
				pDevice->Release();
				pSwapChain3->Release();
				pSrvDescHeap->Release();
				return;
			}

			SIZE_T rtvDescriptorSize = pDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
			D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle = pRtvDescHeap->GetCPUDescriptorHandleForHeapStart();
			pCmdAlloc = new ID3D12CommandAllocator * [bufferCount];
			for (int i = 0; i < bufferCount; ++i)
			{
				mainRenderTargets.push_back(rtvHandle);
				rtvHandle.ptr += rtvDescriptorSize;
			}
		}

		for (UINT i = 0; i < sc_desc.BufferCount; ++i)
		{
			if (pDevice->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&pCmdAlloc[i])) != S_OK)
			{
				pDevice->Release();
				pSwapChain3->Release();
				pSrvDescHeap->Release();
				for (UINT j = 0; j < i; ++j)
				{
					pCmdAlloc[j]->Release();
				}
				pRtvDescHeap->Release();
				delete[]pCmdAlloc;
				return;
			}
		}

		if (pDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, pCmdAlloc[0], NULL, IID_PPV_ARGS(&pCmdList)) != S_OK ||
			pCmdList->Close() != S_OK)
		{
			pDevice->Release();
			pSwapChain3->Release();
			pSrvDescHeap->Release();
			for (UINT i = 0; i < bufferCount; ++i)
				pCmdAlloc[i]->Release();
			pRtvDescHeap->Release();
			delete[]pCmdAlloc;
			return;
		}

		pBackBuffer = new ID3D12Resource * [bufferCount];
		for (UINT i = 0; i < bufferCount; i++)
		{
			pSwapChain3->GetBuffer(i, IID_PPV_ARGS(&pBackBuffer[i]));
			pDevice->CreateRenderTargetView(pBackBuffer[i], NULL, mainRenderTargets[i]);
		}

		ImGui::CreateContext();
		ImGuiIO& io = ImGui::GetIO();
		io.IniFilename = NULL;

		ImGui_ImplDX12_Init(pDevice, bufferCount, DXGI_FORMAT_R8G8B8A8_UNORM, NULL,
			pSrvDescHeap->GetCPUDescriptorHandleForHeapStart(),
			pSrvDescHeap->GetGPUDescriptorHandleForHeapStart());

		get_steam_client()->steam_overlay->CreateFonts();

		initialized = true;

		pDevice->Release();
	}

	ImGui_ImplDX12_NewFrame();

	{
		Windows_Hook::Inst()->prepareForOverlay(sc_desc.OutputWindow);

		ImGui::NewFrame();

		get_steam_client()->steam_overlay->OverlayProc();

		UINT bufferIndex = pSwapChain3->GetCurrentBackBufferIndex();

		D3D12_RESOURCE_BARRIER barrier = {};
		barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
		barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
		barrier.Transition.pResource = pBackBuffer[bufferIndex];
		barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
		barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
		barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;

		pCmdAlloc[bufferIndex]->Reset();
		pCmdList->Reset(pCmdAlloc[bufferIndex], NULL);
		pCmdList->ResourceBarrier(1, &barrier);
		pCmdList->OMSetRenderTargets(1, &mainRenderTargets[bufferIndex], FALSE, NULL);
		pCmdList->SetDescriptorHeaps(1, &pSrvDescHeap);

		ImGui::Render();
		ImGui_ImplDX12_RenderDrawData(ImGui::GetDrawData(), pCmdList);

		barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
		barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
		pCmdList->ResourceBarrier(1, &barrier);
		pCmdList->Close();

		pCmdQueue->ExecuteCommandLists(1, (ID3D12CommandList**)&pCmdList);
	}
	pSwapChain3->Release();
}

HRESULT STDMETHODCALLTYPE DX12_Hook::MyPresent(IDXGISwapChain* _this, UINT SyncInterval, UINT Flags)
{
	DX12_Hook::Inst()->prepareForOverlay(_this);

	return (_this->*DX12_Hook::Inst()->Present)(SyncInterval, Flags);
}

HRESULT STDMETHODCALLTYPE DX12_Hook::MyResizeTarget(IDXGISwapChain* _this, const DXGI_MODE_DESC* pNewTargetParameters)
{
	DX12_Hook::Inst()->resetRenderState();
	return (_this->*DX12_Hook::Inst()->ResizeTarget)(pNewTargetParameters);
}

HRESULT STDMETHODCALLTYPE DX12_Hook::MyResizeBuffers(IDXGISwapChain* _this, UINT BufferCount, UINT Width, UINT Height, DXGI_FORMAT NewFormat, UINT SwapChainFlags)
{
	DX12_Hook::Inst()->resetRenderState();
	return (_this->*DX12_Hook::Inst()->ResizeBuffers)(BufferCount, Width, Height, NewFormat, SwapChainFlags);
}

void STDMETHODCALLTYPE DX12_Hook::MyExecuteCommandLists(ID3D12CommandQueue* _this, UINT NumCommandLists, ID3D12CommandList* const* ppCommandLists)
{
	DX12_Hook* me = DX12_Hook::Inst();
	me->pCmdQueue = _this;

	(_this->*DX12_Hook::Inst()->ExecuteCommandLists)(NumCommandLists, ppCommandLists);
}

DX12_Hook::DX12_Hook() :
	initialized(false),
	pCmdQueue(nullptr),
	bufferCount(0),
	pCmdAlloc(nullptr),
	pSrvDescHeap(nullptr),
	pCmdList(nullptr),
	pRtvDescHeap(nullptr),
	hooked(false),
	Present(nullptr),
	ResizeBuffers(nullptr),
	ResizeTarget(nullptr),
	ExecuteCommandLists(nullptr),
	pDevice(nullptr)
{
    _library = LoadLibrary(DX12_DLL);

	PRINT_DEBUG("DX12 support is experimental, don't complain if it doesn't work as expected.\n");
}

DX12_Hook::~DX12_Hook()
{
	PRINT_DEBUG("DX12 Hook removed\n");

	if (initialized)
	{
		pSrvDescHeap->Release();
		for (UINT i = 0; i < bufferCount; ++i)
		{
			pCmdAlloc[i]->Release();
			pBackBuffer[i]->Release();
		}
		pRtvDescHeap->Release();
		delete[]pCmdAlloc;
		delete[]pBackBuffer;

		ImGui_ImplDX12_InvalidateDeviceObjects();
		ImGui::DestroyContext();

		initialized = false;
	}

	FreeLibrary(reinterpret_cast<HMODULE>(_library));

	_inst = nullptr;
}

DX12_Hook* DX12_Hook::Inst()
{
	if (_inst == nullptr)
		_inst = new DX12_Hook();

	return _inst;
}

const char* DX12_Hook::get_lib_name() const
{
    return DX12_DLL;
}

bool DX12_Hook::loadTexture(const char* filename, void** out_srv, int* out_width, int* out_height) {
#if defined(__WINDOWS_64__)
	// Modified version of https://github.com/ocornut/imgui/wiki/Image-Loading-and-Displaying-Examples
	// Load from disk into a raw RGBA buffer
	int image_width = 0;
	int image_height = 0;
	unsigned char* image_data = loadImageFromFile(filename, &image_width, &image_height);
	if (image_data == NULL)
		return false;

	// Get CPU/GPU handles for the shader resource view
	// Normally your engine will have some sort of allocator for these - here we assume that there's an SRV descriptor heap in
	// g_pd3dSrvDescHeap with at least two descriptors allocated, and descriptor 1 is unused
	UINT handle_increment = pDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
	int descriptor_index = 1; // The descriptor table index to use (not normally a hard-coded constant, but in this case we'll assume we have slot 1 reserved for us)
	D3D12_CPU_DESCRIPTOR_HANDLE my_texture_srv_cpu_handle = pSrvDescHeap->GetCPUDescriptorHandleForHeapStart();
	my_texture_srv_cpu_handle.ptr += (handle_increment * descriptor_index);
	D3D12_GPU_DESCRIPTOR_HANDLE my_texture_srv_gpu_handle = pSrvDescHeap->GetGPUDescriptorHandleForHeapStart();
	my_texture_srv_gpu_handle.ptr += (handle_increment * descriptor_index);

	// Create texture resource
	D3D12_HEAP_PROPERTIES props;
	memset(&props, 0, sizeof(D3D12_HEAP_PROPERTIES));
	props.Type = D3D12_HEAP_TYPE_DEFAULT;
	props.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	props.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;

	D3D12_RESOURCE_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	desc.Alignment = 0;
	desc.Width = image_width;
	desc.Height = image_height;
	desc.DepthOrArraySize = 1;
	desc.MipLevels = 1;
	desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	desc.Flags = D3D12_RESOURCE_FLAG_NONE;

	ID3D12Resource* pTexture = NULL;
	pDevice->CreateCommittedResource(&props, D3D12_HEAP_FLAG_NONE, &desc,
		D3D12_RESOURCE_STATE_COPY_DEST, NULL, IID_PPV_ARGS(&pTexture));

	// Create a temporary upload resource to move the data in
	UINT uploadPitch = (image_width * 4 + D3D12_TEXTURE_DATA_PITCH_ALIGNMENT - 1u) & ~(D3D12_TEXTURE_DATA_PITCH_ALIGNMENT - 1u);
	UINT uploadSize = image_height * uploadPitch;
	desc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	desc.Alignment = 0;
	desc.Width = uploadSize;
	desc.Height = 1;
	desc.DepthOrArraySize = 1;
	desc.MipLevels = 1;
	desc.Format = DXGI_FORMAT_UNKNOWN;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
	desc.Flags = D3D12_RESOURCE_FLAG_NONE;

	props.Type = D3D12_HEAP_TYPE_UPLOAD;
	props.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	props.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;

	ID3D12Resource* uploadBuffer = NULL;
	HRESULT hr = pDevice->CreateCommittedResource(&props, D3D12_HEAP_FLAG_NONE, &desc,
		D3D12_RESOURCE_STATE_GENERIC_READ, NULL, IID_PPV_ARGS(&uploadBuffer));
	IM_ASSERT(SUCCEEDED(hr));

	// Write pixels into the upload resource
	void* mapped = NULL;
	D3D12_RANGE range = { 0, uploadSize };
	hr = uploadBuffer->Map(0, &range, &mapped);
	IM_ASSERT(SUCCEEDED(hr));
	for (int y = 0; y < image_height; y++)
		memcpy((void*)((uintptr_t)mapped + y * uploadPitch), image_data + y * image_width * 4, image_width * 4);
	uploadBuffer->Unmap(0, &range);

	// Copy the upload resource content into the real resource
	D3D12_TEXTURE_COPY_LOCATION srcLocation = {};
	srcLocation.pResource = uploadBuffer;
	srcLocation.Type = D3D12_TEXTURE_COPY_TYPE_PLACED_FOOTPRINT;
	srcLocation.PlacedFootprint.Footprint.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	srcLocation.PlacedFootprint.Footprint.Width = image_width;
	srcLocation.PlacedFootprint.Footprint.Height = image_height;
	srcLocation.PlacedFootprint.Footprint.Depth = 1;
	srcLocation.PlacedFootprint.Footprint.RowPitch = uploadPitch;

	D3D12_TEXTURE_COPY_LOCATION dstLocation = {};
	dstLocation.pResource = pTexture;
	dstLocation.Type = D3D12_TEXTURE_COPY_TYPE_SUBRESOURCE_INDEX;
	dstLocation.SubresourceIndex = 0;

	D3D12_RESOURCE_BARRIER barrier = {};
	barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	barrier.Transition.pResource = pTexture;
	barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
	barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;

	// Create a temporary command queue to do the copy with
	ID3D12Fence* fence = NULL;
	hr = pDevice->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence));
	IM_ASSERT(SUCCEEDED(hr));

	HANDLE event = CreateEvent(0, 0, 0, 0);
	IM_ASSERT(event != NULL);

	D3D12_COMMAND_QUEUE_DESC queueDesc = {};
	queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	queueDesc.NodeMask = 1;

	ID3D12CommandQueue* cmdQueue = NULL;
	hr = pDevice->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&cmdQueue));
	IM_ASSERT(SUCCEEDED(hr));

	ID3D12CommandAllocator* cmdAlloc = NULL;
	hr = pDevice->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&cmdAlloc));
	IM_ASSERT(SUCCEEDED(hr));

	ID3D12GraphicsCommandList* cmdList = NULL;
	hr = pDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, cmdAlloc, NULL, IID_PPV_ARGS(&cmdList));
	IM_ASSERT(SUCCEEDED(hr));

	cmdList->CopyTextureRegion(&dstLocation, 0, 0, 0, &srcLocation, NULL);
	cmdList->ResourceBarrier(1, &barrier);

	hr = cmdList->Close();
	IM_ASSERT(SUCCEEDED(hr));

	// Execute the copy
	cmdQueue->ExecuteCommandLists(1, (ID3D12CommandList* const*)&cmdList);
	hr = cmdQueue->Signal(fence, 1);
	IM_ASSERT(SUCCEEDED(hr));

	// Wait for everything to complete
	fence->SetEventOnCompletion(1, event);
	WaitForSingleObject(event, INFINITE);

	// Tear down our temporary command queue and release the upload resource
	cmdList->Release();
	cmdAlloc->Release();
	cmdQueue->Release();
	CloseHandle(event);
	fence->Release();
	uploadBuffer->Release();

	// Create a shader resource view for the texture
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc;
	ZeroMemory(&srvDesc, sizeof(srvDesc));
	srvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = desc.MipLevels;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	pDevice->CreateShaderResourceView(pTexture, &srvDesc, my_texture_srv_cpu_handle);

	// Return results
	*out_srv = &my_texture_srv_gpu_handle.ptr;
	*out_width = image_width;
	*out_height = image_height;
	freeImage(image_data);

	return true;
#else
	return false;
#endif
}

void DX12_Hook::loadFunctions(ID3D12CommandQueue* pCommandQueue, IDXGISwapChain* pSwapChain)
{
	void** vTable;

	vTable = *reinterpret_cast<void***>(pCommandQueue);
#define LOAD_FUNC(X) (void*&)X = vTable[(int)ID3D12CommandQueueVTable::X]
	LOAD_FUNC(ExecuteCommandLists);
#undef LOAD_FUNC

	vTable = *reinterpret_cast<void***>(pSwapChain);
#define LOAD_FUNC(X) (void*&)X = vTable[(int)IDXGISwapChainVTable::X]
	LOAD_FUNC(Present);
	LOAD_FUNC(ResizeBuffers);
	LOAD_FUNC(ResizeTarget);
#undef LOAD_FUNC
}

#endif//EMU_OVERLAY